# Real-world Restaurant
Customers can visit restaurant and enjoy delicious food. Waiter are helping them to choose the menu. Our chefs are expert in cooking.  
There are four roles in our project:
1. Customer
2. Waiter
3. Chef
4. Owner

## References
* [Dominos Pizza](http://dominos.com)

## Architecture
* ![Architecture](/architecture.jpg)
